package NP;
import static org.junit.Assert.*;
//import org.junit.After;
//import org.junit.Before;
import org.junit.Test;

public class NPModelTest {
	
	@Test
	  public void testGetNP1()
	  {
	    NPModel c = new NPModel();
	    assertEquals("true", c.getNP("CA",1));
	  }
	
	@Test
	  public void testGetNP2()
	  {
	    NPModel c = new NPModel();
	    assertEquals("no parks", c.getNP("KY",9));
	  }
	
	@Test
	  public void testGetNP3()
	  {
	    NPModel c = new NPModel();
	    assertEquals("false", c.getNP("12",5));
	  }
	
	@Test
	  public void testGetName()
	  {
	    NPModel c = new NPModel();
	    c.getNP("CA",1);
	    assertEquals("John Muir National Historic Site", c.getName());
	  }
	
	@Test
	  public void testGetDescription()
	  {
	    NPModel c = new NPModel();
	    c.getNP("CA",1);
	    assertEquals("John Muir played many roles in his life, all of which helped him succeed in his role as an advocate for Nature. As America’s most famous naturalist and conservationist, Muir fought to protect the wild places he loved, places we can still visit today. Muir’s writings convinced the U.S. government to protect Yosemite, Sequoia, Grand Canyon and Mt. Rainier as national parks.", c.getDescrip());
	  }
	
	@Test
	  public void testGetWebsite()
	  {
	    NPModel c = new NPModel();
	    c.getNP("CA",1);
	    assertEquals("https://www.nps.gov/jomu/index.htm", c.getWebsite());
	  }
}
