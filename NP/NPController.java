package NP;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;


public class NPController implements Initializable {

  @FXML
  private Button btngo;
  
  @FXML
  private Button btn1;
  
  @FXML
  private Button btn2;
  
  @FXML
  private Button btn3;
  
  @FXML
  private Button btn4;
  
  @FXML
  private Button btn5;
  
  @FXML
  private Button btn6;
  
  @FXML
  private Button btn7;
  
  @FXML
  private Button btn8;
  
  @FXML
  private Button btn9;
  
  @FXML
  private Button btn10;

  @FXML
  private TextField txtstatecode;

  @FXML
  private Label lblname;
  
  @FXML
  private Label lbldescrip;
  
  @FXML
  private WebView webview;
  
  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    NPModel natPark = new NPModel();
    int number = 0;
    switch (e.getSource().toString())
    {
    case "Button[id=btngo, styleClass=button]'GO'": number= 0;break;
    case "Button[id=btn1, styleClass=button]'1'": number= 0;break;
    case "Button[id=btn2, styleClass=button]'2'": number= 1;break;
    case "Button[id=btn3, styleClass=button]'3'": number= 2;break;
    case "Button[id=btn4, styleClass=button]'4'": number= 3;break;
    case "Button[id=btn5, styleClass=button]'5'": number= 4;break;
    case "Button[id=btn6, styleClass=button]'6'": number= 5;break;
    case "Button[id=btn7, styleClass=button]'7'": number= 6;break;
    case "Button[id=btn8, styleClass=button]'8'": number= 7;break;
    case "Button[id=btn9, styleClass=button]'9'": number= 8;break;
    case "Button[id=btn10, styleClass=button]'10'": number= 9;break;
    default: number = 0;break;
    }
    System.out.println(e.getSource().toString());
    System.out.println(number);
    // Has the go button been pressed?
    if (e.getSource() == btngo || e.getSource() == btn1 || e.getSource() == btn2 || e.getSource() == btn3 || e.getSource() == btn4 || e.getSource() == btn5 || e.getSource() == btn6 || e.getSource() == btn7 || e.getSource() == btn8 || e.getSource() == btn9 || e.getSource() == btn10)
    {
      
    	  String stateCode = txtstatecode.getText();
      if (natPark.getNP(stateCode,number).equals("true"))
      {
        lblname.setText(natPark.getName());
        lbldescrip.setText(natPark.getDescrip());
        webview.getEngine().load(natPark.getWebsite());
      }
      else if(natPark.getNP(stateCode,number).equals("no parks"))
      {
    	    lblname.setText("");
        lbldescrip.setText("There are not that many parks in the state you selected.");
        webview.getEngine().load("https://m.youtube.com/watch?v=QH2-TGUlwu4");
      }
      else
      {
    	    lblname.setText("");
        lbldescrip.setText("Please enter a valid state code.");
        webview.getEngine().load("https://m.youtube.com/watch?v=QH2-TGUlwu4");
      }
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
