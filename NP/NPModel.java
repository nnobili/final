package NP;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
//import javafx.scene.image.Image;
//import com.google.gson.*;

public class NPModel {
  private JsonElement jse;
  private final String apiKey = "YFnVUY551o2pV2lJyjn4INJYskaH8C3ILgFM6ikt";
  
public String test = "lat:37.85982543, long:-122.6007386";
int counter = 0;  
public boolean valid = false;

  public String getNP(String state, int number)
  { 
	try
    {
      URL wuURL = new URL("https://developer.nps.gov/api/v1/parks?stateCode=" + state + "&limit=9&api_key=" + apiKey);
      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
    }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }
    
	if (state.length()==2)
	{
	  valid = true;
	}
	
    counter = number;
    // Check to see if the zip code was valid.
    return isValid();
  }

  public String isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
	String stat = "true";  
	 
	  String check = "{\"total\":0,\"data\":[],\"limit\":9.0,\"start\":1.0}";
      String current = jse.toString();
      //System.out.println(current);
      boolean validStateCode = false;
      if (!current.equals(check) && valid==true)
      {
    	  validStateCode = true;
       //System.out.println(Integer.parseInt(jse.getAsJsonObject().get("total").getAsString()));
      if (validStateCode==true && Integer.parseInt(jse.getAsJsonObject().get("total").getAsString())>=counter+1)  
      {	
    	    stat = "true";
      }
      else if(validStateCode == true && Integer.parseInt(jse.getAsJsonObject().get("total").getAsString())<counter+1)
      {
    	    stat = "no parks";
      }
      }
      else 
      {
    	    stat = "false"; 
      }
      return stat;  
  }
  
  public String getName()
  {
    return jse.getAsJsonObject().get("data").getAsJsonArray().get(counter).getAsJsonObject().get("fullName").getAsString();
  }

  public String getDescrip()
  {
	return jse.getAsJsonObject().get("data").getAsJsonArray().get(counter).getAsJsonObject().get("description").getAsString();
  }
/*  
  public String getWeather()
  {
	  return jse.getAsJsonObject().get("data").getAsJsonArray().get(counter).getAsJsonObject().get("fullName").getAsJsonObject().getAsString();
  }
  
  public double getTemp()
  {
    return jse.getAsJsonObject().get(counter).getAsJsonObject().get("temp_f").getAsDouble();
  }
  
  public String getWind()
  {
    return jse.getAsJsonObject().get(counter).getAsJsonObject().get("wind_string").getAsString();
  }
  
  public double getPressure()
  {
    return jse.getAsJsonObject().get(counter).getAsJsonObject().get("pressure_in").getAsDouble();
  }
  
  public double getVisibility()
  {
    return jse.getAsJsonObject().get(counter).getAsJsonObject().get("visibility_mi").getAsDouble();
  }
*/
  public String getWebsite()
  {
	 return jse.getAsJsonObject().get("data").getAsJsonArray().get(counter).getAsJsonObject().get("url").getAsString();
  }
}